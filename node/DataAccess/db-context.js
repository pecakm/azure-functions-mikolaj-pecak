const sql = require('mssql');
const parser = require('mssql-connection-string');

class PeopleDbContext {
    constructor(connectionString, log) {
        log("PeopleDbContext object has been created.");
        this.log = log;
        this.config = parser(connectionString);
        this.getPeople = this.getPeople.bind(this);
        this.addPerson = this.addPerson.bind(this);
    }

    async getPeople() {
        this.log("getPeople function - run")
        const connection = await new sql.ConnectionPool(this.config).connect();
        const request = new sql.Request(connection);
        const result = await request.query('select * from People');
        this.log("getPeople function - done")
        return result.recordset;
    }

    async getPersonById(id) {
        this.log("getPersonById function - run")
        const connection = await new sql.ConnectionPool(this.config).connect();
        const request = new sql.Request(connection);
        const result = await request.query(`select * from People where PersonId = ${id}`);
        this.log("getPersonById function - done")
        return result.recordset;
    }

    async addPerson(firstName, lastName) {
        this.log("addPerson function - run")
        const connection = await new sql.ConnectionPool(this.config).connect();
        const request = new sql.Request(connection);
        const result = await request.query(`insert into People (FirstName, LastName) values ('${firstName}', '${lastName}')`);
        this.log("addPerson function - done")
        return result.recordset;
    }

    async deletePerson(id) {
        this.log("removePerson function - run")
        const connection = await new sql.ConnectionPool(this.config).connect();
        const request = new sql.Request(connection);
        const result = await request.query(`delete from People where PersonId = ${id}`);
        this.log("removePerson function - done")
        return result.recordset;
    }

    async updatePerson(id, firstName, lastName) {
        this.log("updatePerson function - run")
        const connection = await new sql.ConnectionPool(this.config).connect();
        const request = new sql.Request(connection);
        const result = await request.query(`update People set FirstName = '${firstName}', LastName = '${lastName}' where PersonId = ${id}`);
        this.log("updatePerson function - done")
        return result.recordset;
    }
}

module.exports = PeopleDbContext;